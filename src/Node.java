
import java.util.*;

public class Node {

   private String name;
   private Node firstChild;
   private Node nextSibling;

   Node (String n, Node d, Node r) {
      name = n;
      firstChild = d;
      nextSibling = r;
   }

   /* Klassikaaslase poolt abi nii koodi läbi mõtlemiseks kui ka ideede saamiseks, viide:
    * https://git.wut.ee/i231/home5/src/master/src/Node.java
    * Kasutatud materjalid nii ideedeks kui ka mõteteks:
    * http://enos.itcollege.ee/~jpoial/algoritmid/puud.html
    * https://www.seas.gwu.edu/~mtdiab/Courses/CS1112/lectures/module10/module10Supp.html
    */
   public static Node parsePostfix (String s) {
      otsiErrorit(s);
      String[] tokenid = s.split("");
      Stack<Node> torn = new Stack();
      Node element = new Node(null, null, null);
      boolean asendavJuur = false;
      for (int i = 0; i < tokenid.length; i++) {
         String token = tokenid[i];
         if (token.equals("(")) {
            if (asendavJuur) {
               throw new RuntimeException("Üritatakse asendada juurt");
            }
            torn.push(element);
            element.firstChild = new Node(null, null, null);
            element = element.firstChild;
            if (tokenid[i + 1].equals(",")) {
               throw new RuntimeException("Koma peale tippu");
            }
         } else if (token.equals(")")) {
            element = torn.pop();
            if (torn.size() == 0) {
               asendavJuur = true;
            }
         } else if (token.equals(",")) {
            if (asendavJuur) {
               throw new RuntimeException("Üritatakse juurt asendada.");
            }
            element.nextSibling = new Node(null, null, null);
            element = element.nextSibling;
         } else {
            if (element.name == null) {
               element.name = token;
            } else {
               element.name += token;
            }
         }
      }
      return element;
   }

      // Tagastab errori vastavalt stringis sisalduvale.
      public static void otsiErrorit (String s){
         if (s.contains(",,")) {
            throw new RuntimeException("Topelt komad");
         } else if (s.contains("\t")) {
            throw new RuntimeException("Tabulaatorit kasutatud.");
         } else if (s.contains("()")) {
            throw new RuntimeException("Tühi alampuu.");
         } else if (s.contains(" ")) {
            throw new RuntimeException("Avaldises on tühik.");
         } else if (s.contains("((") && s.contains("))")) {
            throw new RuntimeException("Topeltsulud ei tohi olla avaldises.");
         } else if (s.contains(",") && !(s.contains("(") && s.contains(")"))) {
            throw new RuntimeException("Kaks juurt");
         }
      }

   /* Mõtteid: http://www.sunshine2k.de/coding/java/SimpleParser/SimpleParser.html */
   public String leftParentheticRepresentation() {
      StringBuilder sb = new StringBuilder();
      sb.append(name);
      if (firstChild != null) {
         sb.append("(");
         sb.append(firstChild.leftParentheticRepresentation());
         sb.append(")");
      }
      if (nextSibling != null) {
         sb.append(",");
         sb.append(nextSibling.leftParentheticRepresentation());
      }
      return sb.toString();
   }

   public static void main (String[] param) {
      String s = "(B1,C)A";
      Node t = Node.parsePostfix (s);
      String v = t.leftParentheticRepresentation();
      System.out.println (s + " ==> " + v); // (B1,C)A ==> A(B1,C)
   }
}
